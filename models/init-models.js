var DataTypes = require("sequelize").DataTypes;
var _newtable = require("./submit");
var _user = require("./user");
const Sequelize =require('sequelize');
const config = require('../config/config.json')['development'];

function initModels() {
  const sequelize = new Sequelize(config.database, config.username, config.password, config)
  var newtable = _newtable(sequelize, DataTypes);
  var user = _user(sequelize, DataTypes)

  return {
    newtable,
    user
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;

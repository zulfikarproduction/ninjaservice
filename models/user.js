const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {
    sequelize,
    tableName: 'user',
    schema: 'public',
    timestamps: false,
    hooks: {
      beforeCreate: function(user) {
        user.username = user.username.toString().toLowerCase();
      }
    }
  });
};

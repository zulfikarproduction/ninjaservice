var express = require('express');
const { register, login } = require('../controller/LoginController');
var router = express.Router();
const {submit, download, table} = require('../controller/MainController')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/submit', submit);
router.get('/download', download)
router.get('/showtable', table)
router.post('/login', login)
router.post('/register', register)


module.exports = router;


const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken')
var initModels = require("../models/init-models");
var models = initModels();

exports.login=(req,res)=>{
    const {username, password} = req.body
    console.log('password', password)
    let statusCode = 200;
    let message = 'success';
    const responseData = {statusCode, message};
    models.user.findOne(
        {where:[{username}]}
    ).then((userData)=>{
        if(userData){
            const samePass = bcrypt.compareSync(password, userData.password)
            if(samePass){
                let auth_token = jwt.sign(
                    {
                      auth: userData.username,
                    },
                    "tokenjwtninjadirect",
                    {expiresIn: 30},
                );
                responseData.auth_token=auth_token
                res.status(200).json(responseData)
            }
            else{
                statusCode = 500,
                message = 'failed'
                res.status(200).json({statusCode, message})
            }
        }else{
            statusCode = 500,
            message = 'failed'
            res.status(200).json({statusCode, message})
        }
    })
}


exports.register= async(req,res)=>{
    const {username, password, role} = req.body

    const isHave = await models.user.findOne({where:[{username}]})
    if(isHave){
        return res.status(200).json({statusCode:200, message:'user already registered'})
    }
    let hashedPassword = await bcrypt.hash(password, 10);
    console.log('hashedPassword', hashedPassword)
    const repo = await models.user.create({
                    username, password: hashedPassword, role
                })
                if(repo){
                    return res.status(200).json({statusCode:200, message:'success'})
    }

    

}
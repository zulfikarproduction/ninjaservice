const {DateTime} = require('luxon')
var {parse} = require('json2csv');
var initModels = require("../models/init-models");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
var models = initModels();

exports.table = async (req,res) =>{
    const {startdate, enddate} = req.query;
    let data = []
    if(!startdate && !enddate){
        data = await models.newtable.findAll();
    }else{
        const startDate = DateTime.fromFormat(startdate, 'dd-MM-yyyy').toISODate()
    const endDate = DateTime.fromFormat(enddate, 'dd-MM-yyyy').toISODate()
        data = await models.newtable.findAll({
            where:{
                created_at: {
                    [Op.between]: [startDate, endDate],
                   },
            }
        })
    }



    res.status(200).json(data)
}


exports.submit = async (req,res)=>{
    const{
        phone, email, name, store_name, city, isHave
    } = req.body;

    const responseData = {message:"ok", statusCode:200};
    console.log('req',req.body)
    if(
        phone && email && name && store_name && city
    ){
        const insert = await insertSubmit(phone, email, name, store_name, city, isHave)
        if(insert!=200){
            responseData.message('Error');
            responseData.statusCode('500')
        }
    }else{
        responseData.message('please fill the form');
        responseData.statusCode('500')
    }



    return res.status(200).json(responseData)
}

const insertSubmit = async (phone, email, name, store_name, city, isHave) =>{
    const time = DateTime.now().setZone('UTC+7').toISO();
    console.log('time ',time)
    const insert = await models.newtable.create({
        phone, email, name, store_name, city, created_at: time, is_have: isHave
    })
    if(insert.dataValues){
        return 200
    }else{
        return 500
    }
}

exports.download = async(req,res) =>{
    const {startdate, enddate} = req.query;
    let docs 
    console.log(req.query.startdate)
    if(!startdate && !enddate){
        docs = await models.newtable.findAll();
    }else{
        const startDate = DateTime.fromFormat(startdate, 'dd-MM-yyyy').toISODate()
        const endDate = DateTime.fromFormat(enddate, 'dd-MM-yyyy').toISODate()
        docs = await models.newtable.findAll({
            where:{
                created_at: {
                    [Op.between]: [startDate, endDate],
                   },
            }
        })
    }
    const data = docs.map(doc=>{
        // console.log('~~~~~~~~~~~~~~~~test~~~~~~~~~~~~~~~~')
        // console.log(doc.dataValues)
        return doc.dataValues
    })
    var fields = ['phone', 'email', 'name', 'store_name', 'city', 'created_at'];
    var fieldNames = ['id','phone', 'email', 'name', 'store_name', 'city', 'created_at'];
    // var data = json2csv({ data: docs, fields: fields, fieldNames: fieldNames });
    try{
        const csv = parse(data, fields)
        res.attachment('dataregist'+DateTime.now().setZone('UTC+7').toISO()+'.csv');
        res.status(200).send(csv);
    }catch(error){
        res.status(500).send(error.message)
    }
}